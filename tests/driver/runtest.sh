#!/bin/bash -ex
 
# Fake a build
rm -rf gssapi
cp -r /usr/lib64/python3.6/site-packages/gssapi gssapi
cd gssapi
 
# Do something awful
pip3 install nose{,-parameterized} shouldbe k5test
 
# And run the tests
exec python3 -c 'import nose; exit(nose.main())'
